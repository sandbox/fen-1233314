<?php

/**
 * @file civichimp.webhooks.inc
 * Implement Mailchimp Webhook API Handling
 * Admin menu thanks to @D2ev http://drupal.org/node/272044#comment-5042446
 *
 * @see http://apidocs.mailchimp.com/webhooks/
 */

/**
 * Implement a Drupal endpoint for the Mailchimp Webhooks API.
 * The Mailchimp Webhook will POST data to SITENAME/webhook_api
 */
function civichimp_menu() {
  $items['webhook_api'] = array(
    'page callback'   => 'civichimp_webhook_api',
    'access callback' => TRUE,
  );

  $items['admin/settings/civichimp'] = array(
    'title'            => 'CiviChimp',
    'description'      => 'CiviChimp settings.',
    'position'         => 'left',
    'page callback'    => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file'             => 'system.admin.inc',
    'file path'        => drupal_get_path('module', 'system'),
  );

  $items['admin/settings/civichimp/settings_form'] = array(
    'title'            => 'MailChimp API settings', //hide
    'description'      => 'Mailchimp api setings and group integration form.',
    'page callback'    => 'civichimp_settings_form',
    'access arguments' => array('administer site configuration'),
    'type'             => MENU_NORMAL_ITEM
  );

  $items['admin/settings/civichimp/sync_form'] = array(
    'title'            => 'MailChimp synchronization with CiviCRM', //hide
    'description'      => 'Synchronize all Mailchimp groups with CiviCRM group.',
    'page callback'    => 'civichimp_sync_form',
    'access arguments' => array('administer site configuration'),
    'type'             => MENU_NORMAL_ITEM
  );

  return $items;
}

/**
 * Implementing Form API to get API Key and List Name
 */
function civichimp_settings_form() {
 return drupal_get_form('civichimp_settings_element_form');
}
function civichimp_sync_form() {
 return drupal_get_form('civichimp_sync_element_form');
}

function civichimp_settings_element_form($form_state) {

  require_once 'MCAPI.class.php';
  $apikey = variable_get('civichimp_apikey', '');

  $options = array();
  $civichimp_groups = array();

  if ($apikey != '') {
    $api = new MCAPI($apikey);
    $retvalgroups = $api->lists();
    foreach ($retvalgroups['data'] as $group) {
      $options[$group['name']] = $group['name'];
      $civichimp_groups[$group['name']] = $group;
    }
    variable_set('civichimp_groups', $civichimp_groups);
  }
  else {
    variable_del('civichimp_groups');
  }

  $form['civichimp_settings_form_id']['apikey'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Mailchimp API Key'),
    '#description'      => t('Enter the API key from Mailchimp.'),
    '#default_value'    => variable_get('civichimp_apikey', ''),
    '#required'         => TRUE,
  );
  if ($apikey != '') {
    $form['civichimp_settings_form_id']['mailchimp_groups'] = array(
      '#type'           => 'radios',
      '#title'          => t('Group Availabe in Mailchimp'),
      '#options'        => $options,
      '#default_value'  => variable_get('civichimp_selected_group', array()),
      '#description'    => t('Groups Present in Mailchimp - choose one.'),
    );
  }
  $form['civichimp_settings_form_id']['submit'] = array(
    '#type'             => 'submit',
    '#value'            => t('submit'),
    '#submit'           => array('civichimp_settings_form_submit'),
  );
  return $form;
}

function civichimp_sync_element_form($form_state) {

  $form['civichimp_sync_form_id']['submit'] = array(
    '#type'             => 'submit',
    '#value'            => t('Synchroniza With CiviCRM'),
    '#submit'           => array('civichimp_sync_form_submit'),
  );
  return $form;
}

function civichimp_settings_form_submit(&$form, $form_state) {

  // Set API key variable and $chimp_mail_groups which have full structure of maichimp lists
  variable_set('civichimp_apikey', $form_state['values']['apikey']);

  $civichimp_groups = variable_get('civichimp_groups', array());

  // Check for existing CiviCRM group which are not integrated by Mailchimp
  $civichimp_selected_group = $form_state['values']['mailchimp_groups'];
  variable_set('civichimp_selected_group', $civichimp_selected_group);

  // FIXME: This is a good time to update caches of civicrm & mailchimp groups/assocs
  // variable_set('crm_mc_assoc', $crm_mc_assoc);

  drupal_set_message(t("The configuration options have been saved."));
}

/** Webhook API
 * Data POSTed to SITENAME/webhook_api is dispatched to the appropriate handler
 */
function civichimp_webhook_api() {
  $my_key  = 'fb47e9c3d6c732b2c8bc994bc7509c5f-us2';
  if ( !isset($_GET['key']) ){
    wh_log('No security key specified, ignoring request');
  } elseif ($_GET['key'] != $my_key) {
    wh_log('Security key specified, but not correct: '. $_GET['key']);
  } else {
    // process the request
    // wh_log(t('Processing a "@type" request...', array('@type' => $_POST['type'])));

    civicrm_initialize();

    switch($_POST['type']){
    case 'subscribe'  : subscribe($_POST['data']);   break;
    case 'unsubscribe': unsubscribe($_POST['data']); break;
    case 'cleaned'    : cleaned($_POST['data']);     break;
    case 'upemail'    : upemail($_POST['data']);     break;
    case 'profile'    : profile($_POST['data']);     break;
    default:
      wh_log(t('Request type "@type" unknown, ignoring.',
               array('@type' => $_POST['type'])));
    }
  }
  // wh_log('Finished processing request.');
}

/***********************************************
    Helper Functions
***********************************************/
function wh_log($msg){
  //$logfile = 'webhook.log';
  //file_put_contents($logfile,date("Y-m-d H:i:s")." | ".$msg."\n",FILE_APPEND);
  watchdog('civichimp_api',$msg);
}

/**
 * Subscribe
 * If the contact exists, clear the do_not_email and/or on_hold flags.
 * If the contact doesn't exist, create a new contact with the passed email.
 * (Is this the correct behavior?)
 */
function subscribe($data) {
  $email = $data['email'];

  $contact = civichimp_get_contact_by_email($email);
  $contact_id = $contact['contact_id'];

  if ($contact_id) {
    $unset = array();
    if ($contact['do_not_email']) {
      civichimp_set_contact_value($contact_id, 'do_not_email', 0);
      $unset[] = 'do_not_email';
    }
    if ($contact['on_hold']) {
      civichimp_set_contact_value($contact_id, 'do_not_email', 0);
      $unset[] = 'on_hold';
    }
    if ($unset) {
      wh_log(t('subscribe: unset @unset for <@email> cid=@cid',
               array('@unset' => implode(',', $unset),
                     '@email' => $email,
                     '@cid'   => $contact_id)));
    }
    else {
      wh_log(t('subscribe: nothing needed for <@email> cid=@cid',
               array('@email' => $email,
                     '@cid'   => $contact_id)));
    }
  }
  else {
    // FIXME: should this really create a new contact?
    $list       = civichimp_get_list_object();
    $memberinfo = civichimp_get_memberinfo($list->mc_list_id, $email);
    $params     = civichimp_create_contact_params($list, $memberinfo);

    $contact_id = civichimp_create_contact($params);
    wh_log(t('subscribe: created new contact <@email> cid=@cid',
             array('@email' => $email,
                   '@cid'   => $contact_id)));
  }
}

/**
 * Unsubscribe
 * When a Mailchimp user unsubscribes, it is an opt-out, so set "do_not_email"
 */
function unsubscribe($data) {
  $email = $data['email'];

  $contact = civichimp_get_contact_by_email($email);
  $contact_id = $contact['contact_id'];

  civichimp_set_contact_value($contact_id, 'do_not_email', 1);

  wh_log(t('unsubscribe: set "do_not_email" for <@email> cid=@cid',
           array('@email' => $email,
                 '@cid'   => $contact_id)));
}

/**
 * Cleaned
 * Mailchimp "cleans" users from lists that have had too many bounces.
 * Set these users to "on_hold"
 */
function cleaned($data) {
  $email = $data['email'];

  $contact = civichimp_get_contact_by_email($email);
  $contact_id = $contact['contact_id'];

  civichimp_set_contact_value($contact_id, 'on_hold', 1);

  wh_log(t('cleaned: set "on_hold" for <@email> cid=@cid',
           array('@email' => $email,
                 '@cid'   => $contact_id)));
}

/**
 * Upemail
 * When a Mailchimp updates their email, both the old_email and
 * the new_email are transmitted here via the Webook API.
 * Look up the contact by the old_mail value and update to new_email.
 */
function upemail($data) {
  $old_email = $data['old_email'];
  $new_email = $data['new_email'];

  $contact = civichimp_get_contact_by_email($old_email);
  $contact_id = $contact['contact_id'];

  $result = civichimp_create_or_update_email($contact_id, $new_email);

  wh_log(t('upemail @win: cid=@cid: @old ==> @new',
           array('@win' => $result ? 'success' : 'failed',
                 '@cid' => $contact_id,
                 '@old' => $old_email,
                 '@new' => $new_email)));
}

/**
 * Profile
 * Something in the profile has changed.
 * But the POST data appears unusable (or at least undecipherable)
 * So pull new member info and update CiviCRM groups accordingly
 */
function profile($data) {
  $email  = $data['email'];
  $report = array();

  $contact = civichimp_get_contact_by_email($email);
  $contact_id = $contact['contact_id'];

  if ($contact_id) {

    $list = civichimp_get_list_object();
    $merge_template = civichimp_get_merge_template($list);

    // Get array of all CiviCRM groups associated with Mailchimp interest lists
    $crm_mc_assocs = civichimp_get_civicrm_mailchimp_assocs($list);

    // Get the Mailchimp groupings for member and flatten
    $mc_assocs = civichimp_get_mailchimp_assocs($list, $email, $crm_mc_assocs);

    if ($mc_assocs) {
      // Update the CiviCRM contact with the Mailchimp user's groups
      $report = civichimp_update_civicrm_contact($contact_id, $mc_assocs);
    }

    wh_log(t("Profile: report for <@e> (cid=@c): @r",
             array('@e' => $email,
                   '@c' => $contact_id,
                   '@r' => $report ? implode(',',$report) : 'no changes')));
  }
  else {
    // FIXME: Should a contact be created here?
    // FIFME: Can a profile be modified that CiviCR< doesn't have a contact for?
    // FIXME: Answer: by design, no; but that doesn't mean it won't happen.
    wh_log(t("Profile: report for <@e>: no contact_id",
             array('@e' => $email)));
  }
}
