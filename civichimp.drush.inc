<?php
// $Id$

/**
 * @file
 * Drush cron extension to sync CiviCRM groups with Mailchimp mailing list changes
 *
 * Caveats:
 *  Group names cannot contain commas and must be unique
 */

/**
 * Implements hook_drush_command().
 */
function civichimp_drush_command() {
  $items = array();
  $items['civichimp-sync'] = array(
    'description'         => 'Sync CiviCRM with current Mailchimp mailing lists.',
    'drupal dependencies' => array('civichimp','civicrm'),
    'examples'            => array(
      'drush civichimp-sync'      => 'Sync CiviCRM with current Mailchimp mailing lists.',
    ),
    'aliases'     => array('chimp-sync'),
  );
  $items['civichimp-test'] = array(
    'description'         => 'Test Civichimp Fucntions.',
    'arguments'    => array(
      'function'     => 'Function name',
      'arg1'         => 'Optional first argument',
      'arg2'         => 'Optional second argument',
    ),
    'drupal dependencies' => array('civichimp','civicrm'),
    'examples'            => array(
      'drush civichimp-test'      => 'Test Civichimp Fucntions.',
    ),
    'aliases'     => array('chimp-test'),
  );
  return $items;
}

/**
 * Provides manual testing for exploring Mailchimp and CiviCRM data
 */
function drush_civichimp_test ($func=NULL, $arg1=NULL, $arg2=NULL) {
  civicrm_initialize();

  $contact_id = $arg1;
  $list       = civichimp_get_list_object();

  switch ($func) {
  case 'get_contacts':
  case 'contacts':
    $params = array('version' => 3,
                    'contact_id' => array(106724, 123321));
    $results = civicrm_api("Contact",'get',$params);
    if ($results['is_error']) { drush_log($results['error_message'], 'error'); }
    else { print_r($results); }
    break;
  case 'update_civicrm_contact':
  case 'crm_update':
    $mc_assocs = civichimp_get_mailchimp_assocs($list, $arg2);          // arg2 = email
    print_r(civichimp_update_civicrm_contact($arg1, $mc_assocs));       // arg1 = contact_id
    break;
  case 'get_merge_template':
  case 'template':
    print_r( civichimp_get_merge_template($list) );
    break;
  case 'get_contact_contact_merge_vars':
  case 'contact_merge':
    $merge_template = civichimp_get_merge_template($list);
    print_r( civichimp_get_contact_merge_vars($arg1, $merge_template) );// arg1 = contact_id
    break;
  case 'get_civicrm_mailchimp_assocs':
  case 'crm_mc_assocs':
    print_r( civichimp_get_civicrm_mailchimp_assocs($list) );
    break;
  case 'get_civicrm_assocs':
  case 'crm_assocs':
    $crm_mc_assocs = civichimp_get_civicrm_mailchimp_assocs($list);
    print_r(civichimp_get_civicrm_assocs($list, $arg1, $crm_mc_assocs) );       // arg1 = contact_id
    break;
  case 'get_mailchimp_assocs':
  case 'mc_assocs':
    $crm_mc_assocs = civichimp_get_civicrm_mailchimp_assocs($list);
    print_r(civichimp_get_mailchimp_assocs($list, $arg1, $crm_mc_assocs) );     // arg1 = email
    break;
  case 'is_subscribed':
  case 'issub':
    print_r( civichimp_is_subscribed($list->mc_list_id, $arg1) ?
             'Subscribed' : 'Unsubscribed' );                                   // arg1 = email
    break;
  case 'get_memberinfo':
  case 'info':
    print_r( civichimp_get_memberinfo($list->mc_list_id, $arg1) );              // arg1 = email
    break;
  case 'get_list_id':
  case 'listid':
    print_r( ($rv = civichimp_get_list_id($arg1)) ? $rv : "Can't find list='$arg1'" );
    break;
  case 'get_lists':
  case 'lists':
    print_r( civichimp_get_lists($arg1 ? array($arg1) : NULL) );
    break;
  case 'get_list':
  case 'list':
    print_r( civichimp_get_list($arg1) );
    break;
  case 'get_list_names':
  case 'names';
    print_r( civichimp_get_list_names(($arg1 ? array($arg1) : NULL), $arg2 ) );
    break;
  default:
    civichimp_test_usage();
  }
}

function civichimp_test_usage() {
  $self_record = drush_sitealias_get_record('@self');
  if (empty($self_record)) {
    drush_print('No bootstrapped site.');
  }
  else {
    drush_print('The following site is bootstrapped:');
    _drush_sitealias_print_record($self_record);
  }
  drush_print("\nUsage: chimp.script [get_lists [list_id] | get_list [list_id] | get_groups [reset]]");
}


/**
 * Drush callback; sync CiviCRM groups with Mailchimp mailing list changes
 */
function drush_civichimp_sync () {

  // FIXME: this must be manually set per installation
  // @see http://civicrm.org/blogs/cap10morgan/setting-and-getting-custom-field-values-civicrm-hooks
  $custom_fields = array('Groups'      => 'custom_16',
                         'Date'        => 'custom_17',
                         'Status'      => 'custom_18',
                         'MailchimpID' => 'custom_19'
                         );

  // Initialze CiviCRM and add CiviCRM include_path; Requires civicrm.drush.inc
  // FIXME: add local call to civicrm_initialize() if this function doesn't exist
  _civicrm_init();
  require_once 'CRM/Core/BAO/CustomValueTable.php';
  require_once 'CRM/Core/DAO.php';

  // Get list object
  $list = civichimp_get_list_object();

  /** not in use
  // Set date and default grouplist for storing in cache tables at end of updates
  $date      = gmdate("Y-m-d H:i:s");
  $grouplist = array();

  //Get the last Mailchimp database update.
  $sql = "SELECT last_mailchimp_update FROM civichimp_update";
  $dao = CRM_Core_DAO::executeQuery( $sql, CRM_Core_DAO::$_nullArray );
  $dao->fetch( );
  $last_sync = $dao->last_mailchimp_update;
  //drush_print(dt("last_sync=". $last_sync));
  */
  $last_sync = 0;

  $q = civichimp_get_api_object();

  // Get merge_template (FNAME=>first_name, LNAME=>last_name, EMAIL=>email, etc...)
  $merge_template = civichimp_get_merge_template($list);

  // Get array of all CiviCRM groups associated with Mailchimp interest lists
  $crm_mc_assocs = civichimp_get_civicrm_mailchimp_assocs($list);

  $statuses = array('updated', 'subscribed', 'unsubscribed', 'cleaned');

  foreach ($statuses as $status) {

    // Grab all the members who have the current status
    $members = $q->listMembers($list->mc_list_id, $status, $last_sync, 0, 5000 );
    if ($q->errorCode) {
      return drush_set_error(dt('drush_civichimp_sync: listMembers(): @e',
                                array('@e' => $q->errorMessage)));
    }

    // Loop through each Member returned and change their CiviCRM data to reflect the changes at Mailchimp.
    foreach ($members['data'] as $index => $memberinfo) {
      $email = $memberinfo['email'];

      // Get the Mailchimp groupings for member and flatten
      $mc_assocs = civichimp_get_mailchimp_assocs($list, $email, $crm_mc_assocs);

      // Look up associated contact_id
      $contact = civichimp_get_contact_by_email($email);
      $contact_id = $contact['contact_id'];

      switch ($status) {

      case 'subscribed':

        // If contact_id not found, create the contact
        // FIXME: is this the desired behavior?
        if (! $contact_id) {
          $params = civichimp_create_contact_params($list, $memberinfo);
          $contact_id = civichimp_create_contact($params);
        }

      case 'updated':

        if ($contact_id && $mc_assocs) {

          // Update the CiviCRM contact with the Mailchimp user's groups
          $report = civichimp_update_civicrm_contact($contact_id, $mc_assocs);

          if ($report) {
            drush_log(dt("@s report for <@e> (cid=@c): @r",
                         array('@s' => $status,
                               '@e' => $email,
                               '@c' => $contact_id,
                               '@r' => implode(',',$report))), 'ok');
          }
        }
        break;

      case 'unsubscribed':

        // Mailchimp user unsubscribed, so set "do_not_email" for CiviCRM contact
        civichimp_set_contact_value($contact_id, 'do_not_email', 1);
        break;

      case 'cleaned':

        // Mailchimp user bounced to many times; Set CiviCRM contact "on_hold"
        civichimp_set_contact_value($contact_id, 'on_hold', 1);
        break;

      }
      /** not in use
      // Update Mailchimp last sync time in CiviCRM database for current contact
      $groups_string = implode(',', $grouplist);
      $params = array('entityID'               => $contact_id,
                      $custom_fields['Groups'] => $groups_string,
                      $custom_fields['Date']   => $date,
                      $custom_fields['Status'] => strtoupper($status)
                      );
      CRM_Core_BAO_CustomValueTable::setValues($params);
      */
    }// end foreach members
  }  // end foreach statuses

  /** not in use
  // Update the last_mailchimp_update table to the latest time
  $query = "UPDATE civichimp_update SET last_mailchimp_update = '$date'";
  CRM_Core_DAO::executeQuery( $query );
  */

  drush_log(dt("Mailchimp->CiviCRM sync completed."), 'ok');
}
